# EventLoop

This is still in testing mode.
But I have successfully hard coded Event Loop to send the results from the condor batch job fetch directory to /eos/.

I will do a MR (hopefully before December 15th) to athena but in the mean time, one can use this.

In Executable do inside ( driver == "condor" ):

   TString eosPATH="/eos/user/m/mdacunha/CONDOR_output/"+output_dir+"/"; (Whatever!)

   eldriver->submitOnly(job, output_dir);
   --> 
   eldriver->submitOnly(job, output_dir,eosPATH.Data());


Need to redo cmake and make.
